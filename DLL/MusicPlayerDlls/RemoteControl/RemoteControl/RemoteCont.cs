﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;

namespace RemoteControl
{
    /// <summary>
    /// This class a Tcp porokol handler with my music player
    /// </summary>
    public class RemoteCont:IDisposable
    {
        public delegate void DataRecEventHandler(string data);
        TcpListener server;
        Int32 port = 11111;
        IPAddress myIp;
        Byte[] bytes;
        String data;
        String FileName="config.txt";
        //TEST VARIABLE
        public IPAddress[] testIp { get; set; }
        //
        


        /// <summary>
        /// Create a property of Data
        /// </summary>
        /// 
        public IPAddress MyIp
        {
            get
            {
                return this.myIp;
            }
        }
        public String Data
        {
            get
            {
                return data;
            }
        }

       

        public event DataRecEventHandler DataRecEvent;


        /// <summary>
        /// This is a constructor
        /// </summary>
        public RemoteCont()
        {
            bytes= new Byte[256];
            CreateConfigFile();
            
            LoadConfigFile();
           
            


            server = new TcpListener(myIp, port);
        }
        /// <summary>
        /// Get the local ip address and store the myIp 
        /// </summary>
        private void getLocalIp()
        {
            try
            {
                testIp = Dns.GetHostAddresses(Dns.GetHostName());
                this.myIp= testIp[0];
            }
            catch (Exception)
            {

                
            }
           
        }

        public void Run()
        {
            try
            {
                server.Start();
                while (true)
                {

                    //Console.Write("Waiting for a connection... ");
                    TcpClient client = server.AcceptTcpClient();
                   // Console.WriteLine("Connected!");

                    data = null;


                    NetworkStream stream = client.GetStream();

                    int i;

                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Fire();
                       // Console.WriteLine("Received: {0}", data);

                        // Process the data sent by the client.
                        data = data.ToUpper();

                     

                        // Send back a response.
                       

                        
                    }
                    client.Close();

                }
            }
            catch (Exception e)
            {
                
                
                
            }
            finally
            {
                
            }
        }

        protected void Fire()
        {
            if(DataRecEvent!= null)
            {
                DataRecEvent(data);
            }
        }

        public void Dispose()
        {
           server.Stop();
            server = null;

        }
       //This methode return true if file dosent exist  
        


        /// <summary>
        /// Load the config and set the server
        /// </summary>
        public void LoadConfigFile()
        {
            ///If throw exception this is cretea a file;
            StreamReader sr  = new StreamReader(FileName);


            string temp = "0";
            temp = sr.ReadLine();
           
            if (temp!=null && temp.Length > 0)
            {
                try
                {
                    myIp = IPAddress.Parse(temp);
                }
                catch (Exception)
                {

                    getLocalIp();
                }
            }else
            {
                getLocalIp();
            }
            
            sr.Close();
        }
        /// <summary>
        /// If the config file is not exist this methode will create
        /// </summary>
        private void CreateConfigFile() {
            //I have to call close methode cause the File.Create block the file and I need open for a StreamReader
            if (!File.Exists(FileName))
            {
                FileStream fs = File.Create(FileName);
                fs.Close();
            }
           
        }





    }
}
