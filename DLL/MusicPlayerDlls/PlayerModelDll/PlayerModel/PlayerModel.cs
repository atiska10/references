﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using System.Threading.Tasks;
using System.Timers;


using WMPLib;


namespace PlayerModel
{

    /// <summary>
    /// This is a WindwosMediaPlayer embededd player class
    /// </summary>
    public class PlayerModel : IDisposable
    {




        #region Variables
        public WMPLib.WindowsMediaPlayer player;
        string[] path;
        bool isRunning;
        double poz;
        string part;
        int current;
        int maxPlaylist;
        Timer t;
        double duration;
        //Win api return storage variable
        short key;

        //winapi key thread





        #endregion


        #region Propertys
        public double Poz
        {
            get
            {
                return this.poz;
            }
            set
            {
                this.poz = value;
            }
        }

        public double Duration { get; }

        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
        }


        public string[] Path
        {
            get
            {
                return this.path;
            }
        }


        public int MaxPlaylist
        {
            get
            {
                return maxPlaylist;
            }

            set
            {
                maxPlaylist = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Constructor this is not doing anithyng
        /// </summary>
        public PlayerModel()
        {
            player = new WMPLib.WindowsMediaPlayer();
            poz = 0;
            t = new Timer(1200);

            t.Elapsed += Elapsed;

            //winapi.Start();
            //this function not working well because this funcktion is disabled for a while









        }
        /// <summary>
        /// Play metodh this is call a WINlib methode
        /// </summary>
        public void Play()
        {
            try
            {
                //call the isAMp3() methode that check the file is an .mp3 
                bool go = isAMp3();
                //start the timer
                t.Start();

                if (isRunning == false && go == true)
                {
                    isRunning = true;
                    player.URL = path[MaxPlaylist];
                    player.controls.currentPosition = poz;
                    player.controls.play();

                }
                //if the current url is changed start a new music
                else if (player.URL != path[MaxPlaylist])
                {
                    player.URL = path[MaxPlaylist];
                    player.controls.play();
                    isRunning = true;




                }
                //if the music is running then click the play button the is pause the music
                else if (isRunning == true && go == true)
                {
                    Pause();
                }




            }
            catch (IndexOutOfRangeException)
            {

                maxPlaylist = 0;
                current = maxPlaylist;
            }




        }
        /// <summary>
        /// Pause methoder same as Play
        /// </summary>
        public void Pause()
        {

            if (player.URL == path[maxPlaylist])
            {
                poz = player.controls.currentPosition;
            }


            player.controls.pause();
            isRunning = false;




        }
        /// <summary>
        /// Next Music
        /// </summary>
        /// <param name="path"></param>
        public void setNextMusic(string[] path)
        {
            this.path = path;
        }
        /// <summary>
        /// Check the file is an mp3 file
        /// </summary>
        /// <returns>If the file is not mp3 throw a notMp3Except</returns>
        private bool isAMp3()
        {
            string[] temp = path[0].Split('\\');
            temp = temp[temp.Length - 1].Split('.');
            part = temp[temp.Length - 1];
            if (part != "mp3")
            {
                throw new notMp3Except();

            }
            return true;
        }
        /// <summary>
        /// This methode increase a maxPlaylist and this handle the playlist
        /// </summary>
        public void next()
        {
            if (path != null && path.Length > 1 && MaxPlaylist < path.Length)
            {
                maxPlaylist++;
                if (maxPlaylist == path.Length)
                {
                    maxPlaylist = 0;
                }
            }
            player.URL = path[maxPlaylist];

            GetDuration();
            player.controls.play();




        }
        /// <summary>
        /// Listener for 1,2 second tick and if a music ended the methode call the next() methode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Elapsed(object sender, ElapsedEventArgs e)
        {
            if (player.playState == WMPLib.WMPPlayState.wmppsStopped)
            {
                next();




            }



        }
        /// <summary>
        /// Set the music position at the end of the stream
        /// </summary>
        public void setPositionAtEnd()
        {
            try
            {
                player.controls.currentPosition = player.currentMedia.duration - 10;
            }
            catch (NullReferenceException)
            {

                throw new Exception();
            }

        }
        /// <summary>
        /// Get the duration of the music and store the local variable
        /// </summary>
        public void GetDuration()
        {
            duration = player.currentMedia.duration;
        }

        public void Dispose()
        {
            player.close();

        }
        #endregion

       

        [DllImport("User32.dll")]
        private static extern short GetAsyncKeyState(System.Int32 vKey);



    

    }
}
