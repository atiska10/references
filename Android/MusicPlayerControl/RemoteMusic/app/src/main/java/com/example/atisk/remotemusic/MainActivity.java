package com.example.atisk.remotemusic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {
    Button btn;
    Button pause;
    Button volUp;
    Button volDown;
    Client cli;
    MyT t;
    CharSequence defaultIp="192.168.0.106";

    EditText ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //EDIT TEXT searc by id

        ip=(EditText)findViewById(R.id.Ip);

        ip.setText(defaultIp);



        //Search button by id
        btn=(Button)findViewById(R.id.Btn);
        pause=(Button)findViewById(R.id.Pause);
        volUp=(Button)findViewById(R.id.VolUp);
        volDown=(Button)findViewById(R.id.VolDown);



        //Button's on click event
        volDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t=new MyT("dow",ip.getText().toString());

                t.start();
            }
        });

        volUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t=new MyT("up",ip.getText().toString());
                t.start();

            }
        });


        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence text = "Next!";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                t=new MyT("p",ip.getText().toString());
                t.start();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            //This is a next button
            public void onClick(View v) {
                CharSequence text = "Next!";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
               t=new MyT("n",ip.getText().toString());
                t.start();


            }
        });

    }


}
