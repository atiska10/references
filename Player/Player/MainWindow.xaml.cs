﻿using System;
using System.Windows;
using System.Windows.Media;
using PlayerModel;
using RemoteControl;

using System.Threading;

namespace Player
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string[] pathF;
        PlayerModel.PlayerModel p;
        Thread work;
        


        RemoteControl.RemoteCont server;

        #region WPF Event
        /// <summary>
        /// This is create a WPF window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
          
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            GridComp.Background = Brushes.White;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] part;
                pathF = (string[])e.Data.GetData(DataFormats.FileDrop);
                part = pathF[0].Split('\\');
                string text = part[part.Length-1];
                MusicText.Text = (string)text;
                p.setNextMusic(pathF);
                p.Poz = 0;
                
            }

        }

        private void PlayBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (p.IsRunning)
                {
                    PlayBtn.Background = Brushes.Red;
                }else
                {
                    PlayBtn.Background = Brushes.Green;
                }
                p.Play();

            }
            catch (notMp3Except )
            {
                PlayBtn.Background = Brushes.White;
                MessageBox.Show("Nem mp3 fálj adot meg", "Error", MessageBoxButton.OK);
            }
            catch(NullReferenceException )
            {
                MessageBox.Show("Hiba", "Error", MessageBoxButton.OK);
                PlayBtn.Background = Brushes.White;
            }
            
           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //string message = "Ez az alkalmazás jelenleg csak .mp3 kiterjesztéses zenéket tud lejátszani. A zenéket drag & drop modszerel húzhatod be az ablakba";
            //MessageBox.Show(message, "Info", MessageBoxButton.OK);
            p = new PlayerModel.PlayerModel();

            //Set the range of the Slider
            SliderComp.Minimum = 0;
            SliderComp.Maximum = 100;

            // set the start value
            SliderComp.Value = 50;
            
            //default volume
            p.player.settings.volume = (int)SliderComp.Value;

            //Subscribe the WMPlib media change event's
            p.player.MediaChange += _WMPOCXEvents_MediaChangeEventHandler;

            //Subscribe the WpLib Music position change event (PrograssBar)
            
            
            //New RemoteController class (this will listen)
            server = new RemoteCont();
            

            
           

            //new thread because the RemoteController freeze the UI 
            work = new Thread(server.Run);
            work.Start();


            //Subscribe DataRecEvent (if the clint send data this will fired and push data to the MediaPlayer)
            server.DataRecEvent += onChange;

 


    }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                p.setPositionAtEnd();
            }
            catch (Exception)
            {

                MessageBox.Show("Nincs fálj betöltve", "Error", MessageBoxButton.OK);
            }
            
        }

        private void Window_DragOver(object sender, DragEventArgs e)
        {
            GridComp.Background = Brushes.Red;
        }

        private void SliderComp_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            p.player.settings.volume = (int)SliderComp.Value;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            p.Dispose();
            server.Dispose();
            work.Abort();
            
           
            
            
        }
        
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                p.next();
                

            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Nincs következő szám mivel nem is adtál meg egy számot se", "Error", MessageBoxButton.OK);
                
            }
           
            



        }
        #endregion





        /// <summary>
        /// Fired when the user clicked the next button and change the music (and of course Android client is same)
        /// </summary>
        /// <param name="Item"></param>
        public  void _WMPOCXEvents_MediaChangeEventHandler(object Item)
        {
            MusicText.Text = p.player.URL;
        }
        
        

       

      /// <summary>
      /// This funcktion is a event from RemoteController.
      /// This is received data from android client via SOCKET 
      /// </summary>
      /// <param name="data"></param>
        private void onChange(string data)
        {
            if (data == "n")
            {
                NextButton_Click(this, null);
            }
            else if (data == "p")
            {
                try
                {
                    p.Play();
                }
                catch (notMp3Except)
                {
                    
                    MessageBox.Show("Nem mp3 fálj adot meg", "Error", MessageBoxButton.OK);
                }
                catch (NullReferenceException)
                {
                    MessageBox.Show("Hiba", "Error", MessageBoxButton.OK);
                    
                }
            }
            else if (data == "up")
            {


                    // Invoke methode can access the UI thred from the another tread cause i have to use there cause this event is in an another thread(REMOTE CONTROLLER)
                     this.Dispatcher.BeginInvoke((Action)(() =>
                     {
                        ChangeVolumeUp(5);
                     }));



            }
            else if (data == "dow")
            {

                //Invoke methode can access the UI thred from the another tread cause i have to use there cause this event is in an another thread(REMOTE CONTROLLER)
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    ChangeVolumeUp(-5);
                }));


            }
        }

        /// <summary>
        /// This methode will set the SLide value (+ or - )
        /// </summary>
        /// <param name="value"></param>
        private void ChangeVolumeUp(double value) {
            this.SliderComp.Value += value;
        }
        



       
        

        
    }
}
